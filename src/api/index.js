import { safeLoad } from "js-yaml";
import { includes, map } from "lodash";
import { authToken } from "../utils";

const serverUrl = process.env.REACT_APP_GITLAB_URL;

export const userInfo = async () => {
  var apiUrl = `${serverUrl}/api/v4/user`;
  try {
    const response = await fetch(apiUrl, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + authToken(),
        "Content-Type": "application/json",
      },
    });

    if (response.ok) return await response.json();
  } catch (error) {
    return Promise.reject(error);
  }
};

export const fetchFormData = async (
  project,
  branch,
  config = ".bod-ui.yml"
) => {
  const targetProject = encodeURIComponent(project);
  const uiConfig = encodeURIComponent(config);
  var apiUrl = `${serverUrl}/api/v4/projects/${targetProject}/repository/files/${uiConfig}/raw?ref=${branch}`;

  try {
    const response = await fetch(apiUrl, {
      method: "GET",
      headers: {
        Authorization: "Bearer " + authToken(),
        "Content-Type": "application/json",
      },
    });
    if (response.ok) {
      const data = await response.text();
      var parsed_data = safeLoad(data) || {};

      if (parsed_data.except && includes(parsed_data.except, branch)) {
        return Promise.reject({
          status: 401,
          statusText: `Not allowed to trigger build on ${branch} branch`,
        });
      }
      return Promise.resolve(parsed_data);
    } else return Promise.resolve({});
  } catch (error) {
    const status = error.status || 500;
    const statusText = error.statusText || error.message;
    return Promise.reject({ status, statusText });
  }
};

export const triggerBuild = async (userFormData) => {
  const targetProject = userFormData.project;
  const targetBranch = userFormData.branch;

  // Remove project and branch from the variables
  const formData = { ...userFormData };
  delete formData.project;
  delete formData.branch;

  /*
    Parse the formData to transform it to data comprehensible to gitlab form
    i.e:
    {
        project: 'project1',
        branche: 'branch1',
        variable1: value1;
        variable2: value2,
        variable3: object
    }

    becomes
    [
      {key: variable1, value: value1},
      {key: variable2, value: value2},
      {key: variable2, value: value2},
    ]
  */
  var apiUrl = `${serverUrl}/api/v4/projects/${encodeURIComponent(
    targetProject
  )}/pipeline`;
  const variables = map(formData, (value, key) => {
    return {
      key: key,
      value:
        typeof value === "object" ? JSON.stringify(value) : value.toString(),
    };
  });
  console.log("Data submitted: ", variables);
  try {
    const response = await fetch(apiUrl, {
      method: "POST",
      headers: {
        Authorization: "Bearer " + authToken(),
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        ref: targetBranch,
        variables: variables,
      }),
    });
    if (response.ok) return await response.json();

    const errorMessage = await response.json();
    return Promise.reject({
      status: response.status,
      statusText: JSON.stringify(errorMessage.message),
    });
  } catch (error) {
    const status = error.status || 500;
    const statusText = error.statusText || error.message;
    return Promise.reject({ status, statusText });
  }
};
