import React from "react";
import ReactDOM from "react-dom";
import "./assets/bootstrap/darkyeti.bootstrap.min.css";
import "./index.css";
import App from "./App";

var div_root = document.getElementById("root");
ReactDOM.render(<App />, div_root);
